package main

import (
	"fmt"
	"time"
)

func FibonacciRecursion(n int) int {
	if n <= 1 {
		return n
	}
	return FibonacciRecursion(n-1) + FibonacciRecursion(n-2)
}

func iteration(n int) int64 {
	x := time.Now().UnixNano()
	fmt.Println("\nFib: ", FibonacciRecursion(n))
	y := time.Now().UnixNano()
	diff := y - x
	fmt.Println("x: ", x, "\ny: ", y, "\ndiff: ", diff)
	return diff
}

func main() {
	var count int = 20
	var sum int64 = 0
	for i := 0; i < count; i++ {
		sum += iteration(40)
	}
	fmt.Println("\nAverage: ", (sum / int64(count)))
}
